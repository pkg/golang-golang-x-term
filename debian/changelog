golang-golang-x-term (0.22.0-1+apertis0) apertis; urgency=medium

  * Sync from debian/trixie.

 -- Apertis CI <devel@lists.apertis.org>  Fri, 07 Mar 2025 20:31:40 +0000

golang-golang-x-term (0.22.0-1) unstable; urgency=medium

  * New upstream version 0.22.0
  * Bump versioned dependencies as per go.mod

 -- Anthony Fok <foka@debian.org>  Sun, 01 Sep 2024 08:36:03 -0600

golang-golang-x-term (0.21.0-1) unstable; urgency=medium

  * New upstream version 0.21.0
  * Bump versioned dependency as per go.mod

 -- Anthony Fok <foka@debian.org>  Wed, 31 Jul 2024 10:11:40 -0600

golang-golang-x-term (0.20.0-1) unstable; urgency=medium

  * New upstream version 0.20.0
  * Bump versioned dependency as per go.mod

 -- Anthony Fok <foka@debian.org>  Mon, 20 May 2024 18:35:45 -0600

golang-golang-x-term (0.19.0-1) unstable; urgency=medium

  * New upstream version 0.19.0
  * Bump versioned dependencies as per go.mod
  * Bump Standards-Version to 4.7.0 (no change)

 -- Anthony Fok <foka@debian.org>  Fri, 26 Apr 2024 02:59:32 -0600

golang-golang-x-term (0.18.0-1) unstable; urgency=medium

  * New upstream version 0.18.0
  * Bump versioned dependency as per go.mod

 -- Anthony Fok <foka@debian.org>  Sun, 24 Mar 2024 17:50:12 -0600

golang-golang-x-term (0.17.0-1) unstable; urgency=medium

  * New upstream version 0.17.0
  * Bump versioned dependency as per go.mod

 -- Anthony Fok <foka@debian.org>  Fri, 23 Feb 2024 19:28:03 -0700

golang-golang-x-term (0.16.0-1) unstable; urgency=medium

  * New upstream version 0.16.0
  * Bump versioned dependency as per go.mod

 -- Anthony Fok <foka@debian.org>  Mon, 29 Jan 2024 14:34:03 -0700

golang-golang-x-term (0.15.0-1) unstable; urgency=medium

  * New upstream version 0.15.0
  * Bump versioned dependency as per go.mod

 -- Anthony Fok <foka@debian.org>  Thu, 07 Dec 2023 01:56:59 -0700

golang-golang-x-term (0.13.0-1) unstable; urgency=medium

  * New upstream version 0.13.0
  * Bump versioned dependency as per go.mod

 -- Anthony Fok <foka@debian.org>  Wed, 01 Nov 2023 02:03:38 -0600

golang-golang-x-term (0.12.0-1) unstable; urgency=medium

  * New upstream version 0.12.0
  * Bump versioned dependency as per go.mod

 -- Anthony Fok <foka@debian.org>  Thu, 12 Oct 2023 01:25:53 -0600

golang-golang-x-term (0.11.0-1) unstable; urgency=medium

  * New upstream version 0.11.0
  * Bump versioned dependency as per go.mod

 -- Anthony Fok <foka@debian.org>  Mon, 09 Oct 2023 17:33:38 -0600

golang-golang-x-term (0.9.0-1) unstable; urgency=medium

  * New upstream version 0.9.0
  * Bump versioned dependencies as per go.mod

 -- Anthony Fok <foka@debian.org>  Thu, 21 Sep 2023 03:15:18 -0600

golang-golang-x-term (0.8.0-1) unstable; urgency=medium

  * New upstream version 0.8.0
  * Bump versioned dependencies as per go.mod

 -- Anthony Fok <foka@debian.org>  Wed, 14 Jun 2023 18:38:37 -0600

golang-golang-x-term (0.3.0-1+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Sat, 01 Apr 2023 01:43:31 +0000

golang-golang-x-term (0.3.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 0.3.0
  * Update Standards-Version to 4.6.2 (no changes)
  * Update Homepage

 -- Shengjing Zhu <zhsj@debian.org>  Mon, 02 Jan 2023 23:17:52 +0800

golang-golang-x-term (0.1.0-1) unstable; urgency=medium

  * Team upload.
  * Switch to upstream tag version in uscan
  * New upstream version 0.1.0

 -- Shengjing Zhu <zhsj@debian.org>  Wed, 26 Oct 2022 15:52:30 +0800

golang-golang-x-term (0.0~git20220526.065cf7b-1) unstable; urgency=medium

  * New upstream version 0.0~git20220526.065cf7b
  * Reorder fields in debian/control and debian/copyright
  * Use dh-sequence-golang instead of dh-golang and --with=golang
  * Bump Standards-Version to 4.6.1 (no change)

 -- Anthony Fok <foka@debian.org>  Sun, 05 Jun 2022 08:30:31 -0600

golang-golang-x-term (0.0~git20210615.6886f2d-1) unstable; urgency=medium

  * New upstream version 0.0~git20210615.6886f2d
  * Bump dependency on golang-golang-x-sys-dev to (>= 0.0~git20210615.665e8c7)
  * Mark library package with "Multi-Arch: foreign"
  * Add myself to the list of Uploaders

 -- Anthony Fok <foka@debian.org>  Mon, 06 Sep 2021 12:48:06 -0600

golang-golang-x-term (0.0~git20201210.2321bbc-1apertis0) apertis; urgency=medium

  * Import from Debian bullseye.
  * Set component to development.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Thu, 15 Apr 2021 14:20:22 +0200

golang-golang-x-term (0.0~git20201210.2321bbc-1) unstable; urgency=medium

  * Team upload.
  * New upstream snapshot.

 -- Shengjing Zhu <zhsj@debian.org>  Tue, 22 Dec 2020 22:49:46 +0800

golang-golang-x-term (0.0~git20201207.ee85cb9-1) unstable; urgency=medium

  * Initial release (Closes: #977019).

 -- Arnaud Rebillout <elboulangero@gmail.com>  Thu, 10 Dec 2020 23:37:02 +0800
